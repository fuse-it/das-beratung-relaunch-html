function openMenu() {
    $('#fs-menu').fadeIn();
    $('body').css("overflow-y", "hidden");
}

function closeMenu() {
    $('#fs-menu').fadeOut();
    $('body').css("overflow-y", "auto");
}

$('.fs-nav-link').click(function(e){
    closeMenu();
});

